from rest_framework.generics import (
    ListApiView,
    RetrieveApiView,
    PostUpdateApiView,
    PostDeleteApiView
)
from posts.models import Post
from rest_framework import serializers
from post.api.serializers import PostSerializer

class PostListApiView(ListApiView):
    queryset= Post.objects.all()
    serializer_class = PostSerializer

class PostDetailApiView(RetriveApiView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    lookup_field= 'slug'
    lookup_url_kwargs="abc"
