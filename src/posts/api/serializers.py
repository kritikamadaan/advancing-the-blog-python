from rest_framework import serializers
from posts.models import Post

class PostListSerializer(ModelSerializer):
    class Meta :
        model = Post
        fields=[
            'id',
            'title',
            'slug',
            'content',
            'publish'
        ]


class PostDetailSerializer(ModelSerializer):
    class Meta :
        model = Post
        fields=[
            'id',
            'title',
            'slug',
            'content',
            'publish'
        ]